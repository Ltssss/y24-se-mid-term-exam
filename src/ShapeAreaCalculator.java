import java.util.Scanner;

public class ShapeAreaCalculator {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("请输入形状名称（正方形、圆形 或 三角形）：");
        String shape = scanner.nextLine().trim().toLowerCase();

        if ("正方形".equals(shape)) {
            System.out.println("请输⼊正⽅形的边⻓：");
            double side = scanner.nextDouble();
            double area = calculateSquareArea(side);
            System.out.println("正⽅形的⾯积是：" + area);
        }
        else if ("圆形".equals(shape)) {
            System.out.println("请输⼊圆形的半径：");
            double radius = scanner.nextDouble();
            double area = calculateCircleArea(radius);
            System.out.println("圆形的⾯积是：" + area);

        }
        else if ("三角形".equals(shape)) {
            System.out.println("请输⼊三⻆形的三边⻓度：");
            double a = scanner.nextDouble();
            double b = scanner.nextDouble();
            double c = scanner.nextDouble();
            double area = calculateTriangleArea(a, b, c);
            System.out.println("三⻆形的⾯积是：" + area);

        }
        else {
            System.out.println("不支持的形状类型。");
        }
        scanner.close();
    }
    public static double calculateSquareArea(double sideLength) {
        return sideLength * sideLength;
    }
    public static double calculateCircleArea(double radius) {
        return Math.PI * radius * radius;
    }
    public static double calculateTriangleArea(double a, double b,
                                               double c) {
        double s = (a + b + c) / 2;
        return Math.sqrt(s * (s - a) * (s - b) * (s - c));
    }


}